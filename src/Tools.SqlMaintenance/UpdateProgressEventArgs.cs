using System;

namespace CodeMind.Tools.SqlMaintenance
{
    /// <summary>
    /// Encapsulates the update progress data for notifying users.
    /// </summary>
    public class UpdateProgressEventArgs : EventArgs
    {
        /// <summary>
        /// Creates a new instance of <see cref="UpdateProgressEventArgs"/> class.
        /// </summary>
        /// <param name="message">Update message to display to the user.</param>
        public UpdateProgressEventArgs(string message) : this(message, 0, 0)
        {
        }

        /// <summary>
        /// Creates a new instance of <see cref="UpdateProgressEventArgs"/> class.
        /// </summary>
        /// <param name="message">Update message to display to the user.</param>
        /// <param name="totalActionCount">Total number of actions to be performed.</param>
        /// <param name="currentActionNumber">Current action number.</param>
        public UpdateProgressEventArgs(string message, int totalActionCount, int currentActionNumber)
        {
            Message = message;
            TotalActionCount = totalActionCount;
            CurrentActionNumber = currentActionNumber;
        }

        /// <summary>
        /// Gets the current action number.
        /// </summary>
        public int CurrentActionNumber { get; private set; }

        /// <summary>
        /// Gets the update message to display to the user.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Gets the total action count.
        /// </summary>
        public int TotalActionCount { get; private set; }
    }
}