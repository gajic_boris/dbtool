using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using CodeMind.Tools.SqlMaintenance.Properties;
using log4net;

namespace CodeMind.Tools.SqlMaintenance
{
    /// <summary>
    /// Encapsulates running of SQL queries using the SQL Server query tool installed on the local machine.
    /// </summary>
    public class SqlQueryRunner
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (SqlQueryRunner));
        private static string _applicationPath;
        private static string _sqlToolPath;
        private readonly string _sqlToolArguments;

        static SqlQueryRunner()
        {
            InitializeSqlToolPath();
        }

        /// <summary>
        /// Creates a new instance of <see cref="SqlQueryRunner"/> class with the connection specified.
        /// </summary>
        /// <param name="connection">A <see cref="Connection"/> to read the properties from.</param>
        public SqlQueryRunner(Connection connection)
        {
            Connection = connection;
            _applicationPath = ApplicationPath.GetApplicationPath();
            _sqlToolArguments = GetSqlToolArguments(Connection);
        }

        /// <summary>
        /// Gets the <see cref="Connection"/> to read the properties from.
        /// </summary>
        public Connection Connection { get; private set; }

        /// <summary>
        /// Runs a single query given as a query string.
        /// </summary>
        /// <param name="query">Query string to run.</param>
        public void RunQuery(string query)
        {
            query = query.Replace('\n', ' ').Replace('\r', ' ');
            if (String.IsNullOrEmpty(query))
            {
                _log.Error("Cannot run empty query");
                throw new ArgumentException(Localization.QueryRunnerEmptyQuery, "query");
            }
            
            var arguments = String.Format("{0} -Q \"{1}\" ", _sqlToolArguments, query);

            RunSqlTool(arguments);
        }

        /// <summary>
        /// Runs a file with SQL queries as a batch.
        /// </summary>
        /// <param name="filePath">Path to SQL query file.</param>
        public void RunQueryFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                _log.ErrorFormat("Query file doesn't exist: {0}", filePath);
                throw new ArgumentException(String.Format(Localization.QueryRunnerFileDoesntExist, filePath), "filePath");
            }

            var arguments = String.Format("{0} -i \"{1}\" ", _sqlToolArguments, filePath);
            
            RunSqlTool(arguments);
        }

        private static string GetSqlToolArguments(Connection connection)
        {
            var arguments = new StringBuilder();

            arguments.AppendFormat("-S {0} ", connection.Server);
            arguments.AppendFormat("-d {0} ", connection.Database);
            if (connection.IntegratedSecurity)
                arguments.Append("-E ");
            else
            {
                arguments.AppendFormat("-U {0} ", connection.UserName);
                arguments.AppendFormat("-P {0} ", connection.Password);
            }
            arguments.Append("-e -b -r1 ");

            return arguments.ToString();
        }

        /// <summary>
        /// Initialize SQL query tool path from the config file or search for it on standard locations if
        /// config file path isn't defined.
        /// </summary>
        private static void InitializeSqlToolPath()
        {
            // We try to load from config file first
            _sqlToolPath = ConfigurationManager.AppSettings["SqlToolPath"];
            
            if (File.Exists(_sqlToolPath))
            {
                _log.DebugFormat(Localization.QueryRunnerToolInitialized, _sqlToolPath);
                return;
            }

            // If the config file path is wrong, try to search for the tool on well known locations
            var programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
            var programFiles86 = programFiles;
            const string x86 = " (x86)";

            if (programFiles.EndsWith(x86))
                programFiles = programFiles.Replace(x86, String.Empty);
            if (!programFiles86.EndsWith(x86))
                programFiles86 = programFiles86 + x86;

            var possibleLocations =
                new[]
                    {
                        @"%ProgramFiles%\Microsoft SQL Server\100\Tools\Binn\SQLCMD.EXE",
                        @"%ProgramFiles(x86)%\Microsoft SQL Server\100\Tools\Binn\SQLCMD.EXE",
                        @"%ProgramFiles%\Microsoft SQL Server\90\Tools\Binn\SQLCMD.EXE",
                        @"%ProgramFiles(x86)%\Microsoft SQL Server\90\Tools\Binn\SQLCMD.EXE"
                    };
            
            foreach (var location in possibleLocations)
            {
                var fixedLocation = 
                    location.Replace("%ProgramFiles%", programFiles).Replace("%ProgramFiles(x86)%", programFiles86);

                if (!File.Exists(fixedLocation)) continue;
                _log.DebugFormat(Localization.QueryRunnerToolInitialized, _sqlToolPath);
                _sqlToolPath = fixedLocation;
                break;
            }
        }

        private static void RunSqlTool(string arguments)
        {
            _log.DebugFormat(Localization.QueryRunnerToolRunning, arguments);

            var startInfo =
                new ProcessStartInfo(_sqlToolPath, arguments)
                    {
                        WorkingDirectory = _applicationPath,
                        UseShellExecute = false,
                        CreateNoWindow = true
                    };
            var process = new Process {StartInfo = startInfo};

            int exitCode;
            try
            {
                process.Start();
                process.WaitForExit();
                
                exitCode = process.ExitCode;
            }
            catch (Exception ex)
            {
                _log.Error(String.Format("Error running SQL Tool process\n\n{0}", arguments), ex);
                throw new SqlToolException(Localization.QueryRunnerToolException, ex);
            }

            if (exitCode == 0) return;
            _log.Error(String.Format("Error running SQL Tool process: {0}\n\n{1}", exitCode, arguments));
            throw new SqlToolException(String.Format(Localization.QueryRunnerToolError, exitCode, arguments));
        }
    }
}