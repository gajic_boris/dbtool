using System.Text;

namespace CodeMind.Tools.SqlMaintenance.Scripting
{
    /// <summary>
    /// Defines the database table to script.
    /// </summary>
    public class TableDefinition
    {
        private string _escapedName;
        private string _name;

        /// <summary>
        /// Gets or sets the value indicating that existence check should be performed on each row when scripting.
        /// </summary>
        public bool CheckIfExists { get; set; }

        /// <summary>
        /// Gets the table name enclosed in angle brackets [].
        /// </summary>
        public string EscapedName
        {
            get { return _escapedName ?? (_escapedName = GetEscapedTableName(Name)); }
        }

        /// <summary>
        /// Gets or sets the table name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                _escapedName = null;
            }
        }

        /// <summary>
        /// Gets or sets the id column name.
        /// </summary>
        public string IdColumnNames { get; set; }

        /// <summary>
        /// Gets or sets the value indicating id column as an identity value.
        /// </summary> 
        public bool IsIdentity { get; set; }

        /// <summary>
        /// Gets or sets the value indicating that the row should be updated if it already exists.
        /// </summary>
        public bool UpdateIfExists { get; set; }

        private static string GetEscapedTableName(string table)
        {
            if (!table.Contains("."))
                return "[" + table + "]";

            var segments = table.Split('.');
            var tableName = new StringBuilder();
            for (var i = 0; i < segments.Length; i++)
            {
                if (i > 0) tableName.Append(".");
                tableName.AppendFormat("[{0}]", segments[i]);
            }
            return tableName.ToString();
        }
    }
}