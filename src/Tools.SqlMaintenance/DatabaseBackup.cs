﻿using System;
using System.Configuration;
using System.IO;
using CodeMind.Tools.SqlMaintenance.Properties;
using Ionic.Zip;

namespace CodeMind.Tools.SqlMaintenance
{
    /// <summary>
    /// Responsible for creating SQL Server Database backups and restoring backups.
    /// </summary>
    public class DatabaseBackup
    {
        private const string BackupScript =
            @"
            BACKUP DATABASE [%DatabaseName%] 
            TO DISK = N'%BackupFilePath%' 
            WITH NOFORMAT, INIT, NAME = N'%DatabaseName% - Full Backup - %Timestamp%', SKIP, NOREWIND, NOUNLOAD, STATS = 10";

        private const string RestoreScript =
            @"
            DECLARE @SQL varchar(max);
            SET @SQL = '';

            SELECT @SQL = @SQL + 'Kill ' + Convert(varchar, SPId) + ';'
            FROM MASTER..SysProcesses
            WHERE DBId = DB_ID('%DatabaseName%') AND SPId <> @@SPId;

            EXEC(@SQL);

            RESTORE DATABASE [%DatabaseName%] 
            FROM  DISK = N'%BackupFilePath%' 
            WITH  FILE = 1, NOUNLOAD, REPLACE, STATS = 10;";

        /// <summary>
        /// Gets or sets the connection information.
        /// </summary>
        public Connection Connection { get; set; }

        /// <summary>
        /// Gets or sets the backup file path used for backup or restore operation.
        /// </summary>
        public string BackupFilePath { get; set; }

        /// <summary>
        /// Performs the backup operation.
        /// </summary>
        public void PerformBackup()
        {
            PerformBackup(Connection, BackupFilePath);
        }

        /// <summary>
        /// Performs the backup operation with the given connection parameters and destination path.
        /// </summary>
        /// <param name="connection">Connection parameters.</param>
        /// <param name="destinationPath">Backup file path.</param>
        public void PerformBackup(Connection connection, string destinationPath)
        {
            if (connection == null) throw new ArgumentNullException("connection");
            if (String.IsNullOrEmpty(destinationPath)) throw new ArgumentNullException("destinationPath");
            if (!connection.IsOnLocalServer())
                throw new ServerScopeException(Localization.BackupLocalOnly);

            // We cannot do backup to My Documents or some other user folder, 
            // because the backup runs under network account, so we backup to available temp file 
            // and then copy to destination
            var tempFilePath = GetTempFileName();

            var script = BackupScript
                .Replace("%DatabaseName%", connection.Database)
                .Replace("%BackupFilePath%", tempFilePath)
                .Replace("%Timestamp%", DateTime.Now.ToString("yyyy-MM-dd-HH:mm:ss"));

            var runner = new SqlQueryRunner(connection);
            runner.RunQuery(script);

            // If backup is successful, we copy the temp file to destination
            if (!File.Exists(tempFilePath)) return;
            File.Copy(tempFilePath, destinationPath, true);
            File.Delete(tempFilePath);

            // Zip the destination file...
            var zipFilePath = Path.ChangeExtension(destinationPath, ".zip");
            using (var zip = new ZipFile(zipFilePath))
            {
                zip.AddFile(destinationPath, String.Empty);
                zip.Save();
            }
            File.Delete(destinationPath);
        }

        /// <summary>
        /// Delete old backups.
        /// </summary>
        /// <param name="defaultDestination"></param>
        public void DeleteOldBackups(string defaultDestination)
        {
            int numberOfBackups;
            if (!int.TryParse(ConfigurationManager.AppSettings["Backup.NumberOfBackups"], out numberOfBackups) ||
                numberOfBackups == 0) return;

            var files = Directory.GetFiles(defaultDestination, "*" + Connection.Database + "*");
            if (files.Length <= numberOfBackups) return;
            for (var i = 0; i < files.Length - numberOfBackups; i++)
            {
                File.Delete(files[i]);
            }
        }

        private static string GetTempFileName()
        {
            var folder = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "CodeMind\\CodeMind.Tools");
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            var fileName = Guid.NewGuid().ToString();
            return Path.Combine(folder, fileName + ".bak");
        }

        /// <summary>
        /// Performs the restore operation.
        /// </summary>
        public void PerformRestore()
        {
            PerformRestore(Connection, BackupFilePath);
        }

        /// <summary>
        /// Performs the restore operation with the given connection parameters and backup file path.
        /// </summary>
        /// <param name="connection">Connection parameters.</param>
        /// <param name="backupFilePath">Backup file path.</param>
        public void PerformRestore(Connection connection, string backupFilePath)
        {
            if (connection == null) throw new ArgumentNullException("connection");
            if (String.IsNullOrEmpty(backupFilePath)) throw new ArgumentNullException("backupFilePath");
            if (!connection.IsOnLocalServer())
                throw new ServerScopeException(Localization.RestoreLocalOnly);

            // We cannot restore backup from My Documents or some other user folder, 
            // because the backup runs under network account, so we copy the backup to availabe temp file, 
            // and restore from there
            var tempFilePath = GetTempFileName();

            if (Path.GetExtension(backupFilePath).ToLower() == ".zip")
            {
                // Unpack here to temp file path
                using (var zip = ZipFile.Read(backupFilePath))
                {
                    var outputPath = Path.GetDirectoryName(tempFilePath);
                    zip.ExtractAll(outputPath, ExtractExistingFileAction.OverwriteSilently);
                    var fileName = Path.ChangeExtension(Path.GetFileName(backupFilePath), ".bak");
                    tempFilePath = Path.Combine(outputPath, fileName);
                }
            }
            else File.Copy(backupFilePath, tempFilePath);

            var script = RestoreScript
                .Replace("%DatabaseName%", connection.Database)
                .Replace("%BackupFilePath%", tempFilePath)
                .Replace("%Timestamp%", DateTime.Now.ToString("yyyy-MM-dd-HH:mm:ss"));

            // We must run restore from master database, not the one we're restoring to
            var database = connection.Database;
            connection.Database = "master";

            var runner = new SqlQueryRunner(connection);
            try
            {
                runner.RunQuery(script);
            }
            finally
            {
                // Restore is finished, delete temporary file
                File.Delete(tempFilePath);
                connection.Database = database;
            }
        }
    }
}